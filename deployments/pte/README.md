- Training runs use data from [PTE_TRAINING_MODEL](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.pte_training_model)
- Scoring runs use data from [PTE_SCORING_MODEL](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.pte_scoring_model)
- Both of the above tables use the [pte_base_query.sql macro](https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/macros/workspaces/pte_base_query.sql) to populate data. PTE_TRAINING_MODEL only uses data up until 3 months from yesterday. PTE_SCORING_MODEL uses data up until yesterday.
- [PtE Project](https://gitlab.com/gitlab-data/propensity-to-buy)

