## install anaconda

echo "Installing anaconda.."
curl -O https://repo.anaconda.com/archive/Anaconda3-2021.05-MacOSX-x86_64.sh
bash Anaconda3-2021.05-MacOSX-x86_64.sh -u -b -f
rm Anaconda3-2021.05-MacOSX-x86_64.sh

conda init
rm -R py-earth &> /dev/null

# Ignore the crazy errors from this build, they are expected.
git clone -b issue191 git://github.com/jcrudy/py-earth.git
cd py-earth && python setup.py -q install --cythonize

rm -Rf ${HOME}/anaconda3/bin/pyearth && mv pyearth ${HOME}/anaconda3/bin/
cd ../ && rm -Rf py-earth/

echo "anaconda installed succesfully"
