## Data Team

The [handbook](https://about.gitlab.com/handbook/business-ops/data-team/) is the single source of truth for all of our documentation. 
Please see the [Jupyter Guide](https://about.gitlab.com/handbook/business-technology/data-team/platform/jupyter-guide/) for info on how to get started.  

### Contributing

We welcome contributions and improvements, please see the [contribution guidelines](CONTRIBUTING.md).

### License

This code is distributed under the MIT license, please see the [LICENSE](LICENSE) file.
